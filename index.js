// const http = require('http');
// const path = require('path');
const express = require ('express')
// const req = require('express/lib/request')
// const res = require('express/lib/response')
let users = require('./db/users.json')
// const engine = require('ejs');
const app = express()
const port = 3000

// app.engine('html', engine.__express);
// app.set('views', path.join(__dirname, './views'));
// app.set('view engine', 'html');
app.set('view engine', 'ejs')

app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.get('/', (req, res) => {
    res.render('login')
})

// app.get('/users', (req, res) => {
//     res.status(200).json(users)
// })

//Mengecek akun yang terdaftar dalam users
app.post('/', (req, res) => {
    const email = req.query.email

    let user = users.find(i => i.email === +req.params.email)

    // const {email, password} = req.body

    // let user = users.find(i => i.email === +req.params.email)
    // res.status(200).json(user)

    if (user < 0) {
        res.send("Maaf anda tidak terdaftar")
    } else {
        res.redirect('/home')
    }
})

app.get('/game', (req, res) => {
    res.render('game')
})

app.get('/home', (req, res) => {
    res.render('home')
})

app.listen(port, () => {
    console.log(`App listen on http://localhost:${port}`)
})