class Start {
    constructor() {
        this.playerName = "Player1"
        this.botName = "COM"
        this.playerOption;
        this.botOption;
        this.winner = ""
    }

    get getBotOption() {
        return this.botOption;
    }

    /**
     * @param {string} option
     */
    set setBotOption(option) {
        this.botOption = option;
    }

    get getPlayerOption() {
        return this.playerOption
    }

    /**
     * @param {any} option
     */
    set setPlayerOption(option) {
        this.playerOption = option;
    }

    botBrain() {
        const option = ["Rock", "Paper", "Scissors"];
        const bot = option[Math.floor(Math.random() * option.length)];
        return bot;
    }

    winCalculation() {
        if (this.botOption == "Paper" && this.playerOption == "Scissors") {
            return this.winner = this.playerName
        } else if (this.botOption == "Paper" && this.playerOption == "Rock") {
            return this.winner = this.botName;
        } else if (this.botOption == "Scissors" && this.playerOption == "Paper") {
            return this.winner = this.botName;
        } else if (this.botOption == "Scissors" && this.playerOption == "Rock") {
            return this.winner = this.playerName
        } else if (this.botOption == "Rock" && this.playerOption == "Paper") {
            return this.winner = this.playerName
        } else if (this.botOption == "Rock" && this.playerOption == "Scissors") {
            return this.winner = this.botName;
        } else {
            return this.winner = "Draw"
        }
    }

    matchResult() {
        if (this.winner != "Draw") {
            return `${this.winner} win`;
        } else {
            return `${this.winner}`;
        }
    }
}

function clickOption(params) {
    const start = new Start();
    start.setPlayerOption = params;
    start.setBotOption = start.botBrain();
    start.winCalculation();

    const result = document.getElementById("result");
    document.getElementById("result").style.backgroundColor = "green";
    document.getElementById("result").style.fontSize = "2rem";
    document.getElementById("result").style.color = "white";
    document.getElementById("result").style.rotate = "28.87"

    setTimeout(() => {
        result.textContent = start.matchResult();
    }, 1000);

}
